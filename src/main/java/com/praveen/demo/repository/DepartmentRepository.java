package com.praveen.demo.repository;

import org.springframework.stereotype.Repository;

import com.praveen.demo.domain.DepartmentEntity;

@Repository
public interface DepartmentRepository extends BaseRepository<DepartmentEntity> {

}
