package com.praveen.demo.repository;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import com.praveen.demo.domain.BaseEntity;

@NoRepositoryBean
@Transactional
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T, Long> {

    @Query("update #{#entityName} e set e.isDeleted = true where e.id = ?1")
    @Modifying
    Integer softDeleteById(@NotNull final Long id);

}
