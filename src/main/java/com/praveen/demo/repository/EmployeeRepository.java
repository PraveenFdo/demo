package com.praveen.demo.repository;

import org.springframework.stereotype.Repository;

import com.praveen.demo.domain.EmployeeEntity;

@Repository
public interface EmployeeRepository extends BaseRepository<EmployeeEntity> {

	EmployeeEntity findByfName(String name);

}
