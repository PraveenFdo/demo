package com.praveen.demo.repository;

import com.praveen.demo.domain.EmployeeDepartmentEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDepartmentRepository extends BaseRepository<EmployeeDepartmentEntity> {
}
