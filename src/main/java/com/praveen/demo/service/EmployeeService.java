package com.praveen.demo.service;

import java.util.Collection;

import com.praveen.demo.ui.request.model.EmployeeRequestModel;
import com.praveen.demo.ui.response.model.EmployeeResponseModel;

public interface EmployeeService {

	EmployeeResponseModel save(EmployeeRequestModel employeeRequestModel);

	Collection<EmployeeResponseModel> getAll();

}
