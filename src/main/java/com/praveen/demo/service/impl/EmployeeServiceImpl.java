package com.praveen.demo.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.praveen.demo.domain.EmployeeEntity;
import com.praveen.demo.repository.EmployeeRepository;
import com.praveen.demo.service.EmployeeService;
import com.praveen.demo.ui.request.model.EmployeeRequestModel;
import com.praveen.demo.ui.response.model.EmployeeResponseModel;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository employeeRepository;
	private final ModelMapper mapper;

	@Override
	public Collection<EmployeeResponseModel> getAll() {
		return mapToResponseModel(employeeRepository.findAll());
	}

	@Override
	public EmployeeResponseModel save(EmployeeRequestModel employeeRequestModel) {
		EmployeeEntity employeeEntity = mapToEntity(employeeRequestModel);
		return mapToResponseModel(employeeRepository.save(employeeEntity));
	}

	/* Utility methods for mappings */
	public EmployeeResponseModel mapToResponseModel(EmployeeEntity employeeEntity) {
		return mapper.map(employeeEntity, EmployeeResponseModel.class);
	}

	public EmployeeEntity mapToEntity(EmployeeRequestModel employeeRequestModel) {
		return mapper.map(employeeRequestModel, EmployeeEntity.class);
	}

	public Collection<EmployeeResponseModel> mapToResponseModel(Collection<EmployeeEntity> employeeEntities) {

		Collection<EmployeeResponseModel> employeeResponseModels = new ArrayList<>(employeeEntities.size());

		employeeEntities.forEach(employeeEntity -> {
			EmployeeResponseModel responseModel = mapper.map(employeeEntity, EmployeeResponseModel.class);
			employeeResponseModels.add(responseModel);
		});

		return employeeResponseModels;
	}

}
