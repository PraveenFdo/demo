package com.praveen.demo.domain;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeEntity extends BaseEntity {

	private static final long serialVersionUID = -1471954807638698298L;

	private String fName;
	private String lName;

}
