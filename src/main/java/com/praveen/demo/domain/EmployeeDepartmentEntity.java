package com.praveen.demo.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDepartmentEntity extends BaseEntity {

	private static final long serialVersionUID = -9138582482706656078L;

	@ManyToOne(optional = false)
	private EmployeeEntity employeeEntity;

	@ManyToOne(optional = false)
	private DepartmentEntity departmentEntity;

}
