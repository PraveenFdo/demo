package com.praveen.demo.domain;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentEntity extends BaseEntity {

	private static final long serialVersionUID = 693992328605889036L;

	private String code;
	private String name;

}
