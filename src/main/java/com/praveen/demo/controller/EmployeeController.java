package com.praveen.demo.controller;

import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.praveen.demo.service.EmployeeService;
import com.praveen.demo.ui.response.model.EmployeeResponseModel;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeController {

	private final EmployeeService employeeService;

	@GetMapping
	public Collection<EmployeeResponseModel> getAll() {
		return employeeService.getAll();
	}

}
