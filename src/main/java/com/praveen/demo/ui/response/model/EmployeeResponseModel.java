package com.praveen.demo.ui.response.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeResponseModel {

	private Long id;
	private String fName;
	private String lName;

}
