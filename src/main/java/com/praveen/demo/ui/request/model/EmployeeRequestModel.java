package com.praveen.demo.ui.request.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequestModel {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 30)
    private String fName;

    @NotBlank
    @Size(min = 3, max = 30)
    private String lName;

}
