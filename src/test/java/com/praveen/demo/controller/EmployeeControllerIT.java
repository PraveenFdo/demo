package com.praveen.demo.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.praveen.demo.service.EmployeeService;
import com.praveen.demo.ui.response.model.EmployeeResponseModel;

//import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(EmployeeController.class)
class EmployeeControllerIT {

	@Autowired
	private transient MockMvc mvc;

	@MockBean
	private transient EmployeeService service;

	@BeforeEach
	void setUp() {
		EmployeeResponseModel e1 = new EmployeeResponseModel(1L, "alex", "mendis");
		EmployeeResponseModel e2 = new EmployeeResponseModel(2l, "praveen", "fernando");
		List<EmployeeResponseModel> list = Arrays.asList(e1, e2);
		given(service.getAll()).willReturn(list);
	}

	@Test
	void getAll() throws Exception {
		mvc.perform(get("/employee")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
	}

}