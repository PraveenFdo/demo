package com.praveen.demo.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EmployeeEntityTest {

	private static final String fName = "Frodo";
	private static final String lName = "Baggins";

	private static EmployeeEntity e1;
	private static EmployeeEntity e2;

	@BeforeEach
	void setUp() {
		e1 = new EmployeeEntity(fName, lName);
		e2 = new EmployeeEntity(fName, lName);
	}

	@Test
	void testGettersAndSetters() {
		e1.setFName(fName + "1");
		e1.setLName(lName + "1");

		assertEquals(fName + "1", e1.getFName());
		assertEquals(lName + "1", e1.getLName());
	}

	@Test
	void testEqualsObject() {
		assertTrue(e1.equals(e2));
		assertTrue(e2.equals(e1));
	}

	@Test
	void testHashCode() {
		assertTrue(e1.hashCode() == e2.hashCode());
	}

}
