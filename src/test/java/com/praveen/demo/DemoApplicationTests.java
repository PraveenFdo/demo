package com.praveen.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
//import static org.assertj.core.api.Assertions.*;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void testApplicationStartup() {
		Assertions.assertDoesNotThrow(() -> DemoApplication.main(new String[] { "a", "b" }));
	}

}
