package com.praveen.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.praveen.demo.domain.EmployeeEntity;

@DataJpaTest
public class EmployeeRepositoryIT {

	@Autowired
	private transient TestEntityManager entityManager;

	@Autowired
	private transient EmployeeRepository employeeRepository;

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void findByfName() {
		// given
		EmployeeEntity alex = new EmployeeEntity("alex", "test");
		entityManager.persist(alex);
		entityManager.flush();

		// when
		EmployeeEntity found = employeeRepository.findByfName(alex.getFName());

		// then
		assertThat(found.getFName()).isEqualTo(alex.getFName());
	}

}
