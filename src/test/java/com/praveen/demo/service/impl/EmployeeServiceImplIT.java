package com.praveen.demo.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;

import com.praveen.demo.domain.EmployeeEntity;
import com.praveen.demo.repository.EmployeeRepository;
import com.praveen.demo.service.EmployeeService;
import com.praveen.demo.ui.request.model.EmployeeRequestModel;
import com.praveen.demo.ui.response.model.EmployeeResponseModel;

@SpringBootTest
class EmployeeServiceImplIT {

	@MockBean
	private transient EmployeeRepository employeeRepository;

	@Autowired
	private transient EmployeeService employeeService;

	@TestConfiguration
	class EmployeeServiceImplTestContextConfiguration {

		@Bean
		public ModelMapper modelMapper() {
			ModelMapper mapper = new ModelMapper();
			mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			return mapper;
		}

		@Bean
		public EmployeeService employeeService() {
			return new EmployeeServiceImpl(employeeRepository, modelMapper());
		}
	}

	static String fname = "alex";
	static String lname = "fernando";

	@Test
	void getAll() {
		// given
		EmployeeEntity e1 = new EmployeeEntity(fname, lname);
		EmployeeEntity e2 = new EmployeeEntity(fname, lname);
		List<EmployeeEntity> list = Arrays.asList(e1, e2);

		Mockito.when(employeeRepository.findAll()).thenReturn(list);

		// when
		Collection<EmployeeResponseModel> collection = employeeService.getAll();

		// then
		assertEquals(2, collection.size());
	}

	@Test
	void save() {
		// given
		EmployeeRequestModel requestModel = new EmployeeRequestModel(null, fname, lname);
		EmployeeEntity e1 = new EmployeeEntity(fname, lname);
		EmployeeEntity e2 = new EmployeeEntity(fname, lname);
		e2.setId(2l);

		// given
		Mockito.when(employeeRepository.save(e1)).thenReturn(e2);

		// when
		EmployeeResponseModel responseModel = employeeService.save(requestModel);

		// then
		assertTrue(responseModel.getId() == 2L);
	}

}