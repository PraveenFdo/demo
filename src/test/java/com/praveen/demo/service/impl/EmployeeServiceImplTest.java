package com.praveen.demo.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import com.praveen.demo.domain.EmployeeEntity;
import com.praveen.demo.repository.EmployeeRepository;
import com.praveen.demo.service.EmployeeService;
import com.praveen.demo.ui.request.model.EmployeeRequestModel;
import com.praveen.demo.ui.response.model.EmployeeResponseModel;

//@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTest {
    
    @Mock
    private transient EmployeeRepository employeeRepository;

    @Mock
    private transient ModelMapper mapper;
    
//    @Inject
    private transient EmployeeService employeeService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        employeeService = new EmployeeServiceImpl(employeeRepository, mapper);
    }
    
//    @Test
    void saveTest() {
        // given
        EmployeeRequestModel requestModel = new EmployeeRequestModel(null, "Frodo", "Baggins");
        EmployeeEntity employeeEntity = new EmployeeEntity("Frodo", "Baggins");
        employeeEntity.setId(2l);
        Mockito.when(employeeRepository.save(any())).thenReturn(employeeEntity);

        // when
        EmployeeResponseModel responseModel = employeeService.save(requestModel);

        // then
        assertEquals(2l, responseModel.getId());
    }
    
}
